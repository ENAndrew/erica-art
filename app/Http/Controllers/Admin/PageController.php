<?php

namespace App\Http\Controllers\Admin;

use Image;
use Storage;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Page;

class PageController extends Controller
{
    /**
     * Show the index of pages. 
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {
    	$query = Page::orderBy('created_at', 'desc');

    	if ($request->filled('search')) {
    		$value = "%{$request->input('search')}%";

    		$query->where('synopsis', 'like', $value);
    	}

    	$data['pages'] = $query->paginate(15);

    	return view('admin.pages.index', $data);
    }

    /**
     * Show the form for creating a new page.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return $this->edit(new Page());
    }

    /**
     * Store a newly created page. 
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	return $this->update($request, new Page());
    }

    /**
     * Show the form for editing the page.
     * 
     * @param  \App\Models\Page   $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
    	$this->authorize('update', $page);

    	$data['page'] = $page->load('nextPages');

    	$data['availablePages'] = Page::whereDoesntHave('priorPages')
    		->where('id', '!=', $page->id)
    		->get();

    	return view('admin.pages.edit', $data);
    }

    /**
     * Update the given page in storage. 
     * 
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Page         $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
    	$this->authorize('update', $page);

    	$this->validate($request, [
    		'synopsis' => 'required|string',
    		'text' => 'required|string',
    		'image' => 'nullable|image|max:2500000',
    		'remove_image' => 'nullable|in:on',
    	]);

    	$page->synopsis = $request->input('synopsis');
    	$page->text = $request->input('text');

    	if ($request->file('image')) {
    		$maxWidth = 1920;
    		$maxHeight = 1080;

    		$image = Image::make($request->file('image')->getRealPath());

    		$image->resize($maxWidth, $maxHeight, function ($constraint) {
    			$constraint->aspectRatio();
    			$constraint->upsize();
    		});

    		$hash = $request->file('image')->hashName();
    		$temporaryPath = "/tmp/{$hash}.png";
    		$image->save($temporaryPath);
    		$original = new File($temporaryPath);

    		$page->image_path = Storage::disk('s3')->putFile('images/pages', $original);
    		$page->image_url = Storage::disk('s3')->url($page->image_path);
    	}

    	if ($request->filled('remove_image')) {
    		Storage::disk('s3')->delete($page->image_path);

    		$page->image_path = null;
    		$page->image_url = null;
    	}

    	$saved = $page->save();

    	if ($saved) {
    		return redirect(route('admin.pages.edit', $page))->with('success', 'Page has been saved.');
    	} else {
    		return redirect(route('admin.pages.edit', $page))->with('danger', 'Something went wrong.');
    	}
    }

    /**
     * Destroy the given page in storage.
     * 
     * @param  App\Models\Page   $page
     * @return \Iluminate\Http\Response
     */
    public function destroy(Page $page)
    {
    	$this->authorize('destroy', $page);

    	$deleted = $page->delete();

    	if ($deleted) {
    		return redirect(route('admin.pages.index'))->with('success', 'Page has been deleted.');
    	} else {
    		return redirect(route('admin.pages.index'))->with('danger', 'Something went wrong.');
    	}
    }

    /**
     * Detach a page from the parent page. 
     * 
     * @param  Request $request [description]
     * @return json
     */
    public function asyncDetach(Request $request)
    {
    	$page = Page::where('id', $request->input('parent_id'))->first();

    	$page->nextPages()->detach($request->input('remove_id'));

    	return response()->json([
    		'status' => 200,
    		'message' => 'Page was detached.',
    	]);
    }

    /**
     * Attach a page to the parent page.
     * 
     * @param  \Illuminate\Http\Request $request
     * @return json
     */
    public function asyncAttach(Request $request)
    {
    	$page = Page::where('id', $request->input('parent_id'))->first();
    	$nextPage = Page::where('id', $request->input('attach_id'))->first();

    	$prompt = $request->input('prompt');

    	$page->nextPages()->syncWithoutDetaching(array($nextPage->id => array('prompt' => $prompt)));

    	return response()->json([
    		'status' => 200, 
    		'message' => 'Next page attached.',
    		'new_page' => $nextPage,
    	]);
    }
}
