<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Page;

class SubPageController extends Controller
{
	/**
	 * Show the choose your own adventure page.
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function adventure()
	{
		$data['pageTitle'] = 'Sunshine | A Choose Your Own Adventure';
		$data['metakeywords'] = 'choose your own adventure lovecraft horror writing books';
		$data['metaDescription'] = 'Sunshine Choose Your Own Adventure Game';

		$data['pages'] = Page::orderBy('id', 'asc')->with('nextPages')->get();

		return view('subpages.adventure', $data);
	}

	/**
	 * Show the favoirites page.
	 * 
	 * @return \Illuminate\Http\Response
	 */
    public function favorites() 
    {
    	$data['pageTitle'] = 'Erica Andrew Art | Favorites';
        $data['metaKeywords'] = 'favorite authors movies artists recommendations';
        $data['metaDescription'] = 'Erica\'s Favorite Authors Films and Artists';

        return view('subpages.favorites', $data);
    }
}
