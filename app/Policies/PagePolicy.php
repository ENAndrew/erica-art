<?php

namespace App\Policies;

use Auth;

use App\Models\Page;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;

    /**
     * Allow logged-in users to update pages.
     *
     * @param  App\Models\User   $user
     * @param  App\Models\Page   $page
     * @return boolean
     */
    public function update(User $user, Page $page)
    {
        return $user === Auth::user();
    }

    /**
     * Allow logged-in users to delete pages.
     *
     * @param  App\Models\User   $user
     * @param  App\Models\Page   $page
     * @return boolean
     */
    public function destroy(User $user, Page $page)
    {
        return $user === Auth::user();
    }
}
