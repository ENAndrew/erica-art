<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Events\PageDeleting;

class Page extends Model
{
	/**
	 * The event map for the model.
	 *
	 * @var array
	 */
	protected $dispatchesEvents = [
		'deleting' => PageDeleting::class,
	];

	/**
	 * Return the next pages attached to this page.
	 * 
	 * @return \Illuminate\Eloquent\Database\HasMany
	 */
    public function nextPages()
    {
    	return $this->belongsToMany(Page::class, 'pages_pages', 'page_id', 'next_page_id')->withPivot('prompt');
    }

    /**
     * Return the prior page in the series.
     * 
     * @return \Illuminate\Eloquent\Database\BelongsTo
     */
    public function priorPages()
    {
    	return $this->belongsToMany(Page::class, 'pages_pages', 'next_page_id', 'page_id');
    }
}
