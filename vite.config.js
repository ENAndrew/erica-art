import path from 'path';
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

import vue from "@vitejs/plugin-vue2";

export default defineConfig({
    plugins: [
        laravel({
            input: [
                "resources/js/app.js",
                'resources/sass/app.scss'
            ],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrlsOptions: {
                    base: null,
                    includeAbsolute: false
                }
            }
        }),
    ],
    define: {
        "process.env": process.env,
    },
    resolve: {
        alias: {
            vue: "vue/dist/vue.esm.js",
            "~": "/resources.js",
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
        }
    }
})