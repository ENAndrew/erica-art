/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import '../sass/app.scss';

import Vue from 'vue';
window.Vue = Vue;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

import Adventure from './components/Adventure.vue';
import ImageEditor from './components/ImageEditor.vue';
import ImageGallery from './components/ImageGallery.vue';
import PageAttacher from './components/PageAttacher.vue';
import TinyMCE from './components/TinyMCE.vue';

Vue.component('adventure', Adventure);
Vue.component('image-editor', ImageEditor);
Vue.component('image-gallery', ImageGallery);
Vue.component('page-attacher', PageAttacher);
Vue.component('tinymce', TinyMCE);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
