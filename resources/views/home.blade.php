@extends('layouts.app')

@section('content')
	<div class="home-wrapper wrapper">
		<div class="container text-center" style="padding-top: 120px;">
			<h1>Erica Andrew Art</h1>
		</div>

		<div class="container h-100 text-center">
			<a href="{{ route('digital') }}">
				<picture>
					<source srcset="/img/layout/Moth.webp" type="image/webp">
					<img src="/img/layout/Moth.jpg" class="img-fluid" alt="graphic design of a moth">
				</picture>
			</a>

			<!-- “The effect of this cannot be understood without being there. The beauty of it cannot be understood, either, and when you see beauty in desolation it changes something inside you. Desolation tries to colonize you.” Jeff Vandermeer, Annihilation -->
		</div>
	</div>
@endsection
