@extends ('layouts.admin')

@section ('content')
	<div class="page-edit-wrapper">
		<div class="header clearfix">
			<h1 class="pull-left">@if ($page->id) Edit @else Create @endif Page</h1>

			<a href="{{ route('admin.pages.create') }}">
				<button class="btn btn-teal pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</a>
		</div>

		<form class="basic-form" role="form" action="/admin/pages{{ $page->id ? '/' . $page->id : '' }}" method="POST" autocomplete="force-no" enctype="multipart/form-data">
			@csrf

			@if ($page->id) 
				@method ('PATCH')
			@endif

			@include ('partials.alerts')

			<div class="form-group">
				<label for="synopsis">Synopsis</label>

				<input id="synopsis" class="form-control" type="text" name="synopsis" value="{{ old('synopsis') ?? $page->synopsis }}">
			</div>

			<div class="form-group">
				<label for="text">Page Content</label>

				<tinymce id="text" name="text" value="{{ old('text') ?? $page->text }}"></tinymce>
			</div>

			<div class="form-group">
				<label for="image">Select Image (Optional)</label>

				<input type="file" name="image" class="form-control-file" id="image">
			</div>

			@if ($page->image_url)
				<div class="form-group">
					<label>Current Image</label>

					<img src="{{ $page->image_url }}" class="img-fluid">
				</div>

				<div class="form-check">
					<input class="form-check-input" type="checkbox" name="remove_image" id="remive_image">

					<label class="form-check-label" for="remove_image">Remove Image</label>
				</div>
			@endif

			<div class="form-group">
				<button type="submit" class="btn btn-teal">Submit</button>
			</div>
		</form>

		<hr>

		<page-attacher :page="{{ $page }}" :available-pages="{{ json_encode($availablePages) }}"></page-attacher>
	</div>
@endsection