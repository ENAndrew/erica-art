@extends ('layouts.admin')

@section ('content')
	<div class="page-index-wrapper">
		<div class="header clearfix">
			<h1 class="pull-left">Choose Your Own Pages</h1>

			<a href="{{ route('admin.pages.create') }}">
				<button class="btn btn-teal pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</a>
		</div>

		<div class="mt-5">
			<form class="form">
				<div class="row">
					<div class="col">
						<input class="form-control" name="search" type="text" value="{{ request()->get('search') }}" placeholder="Search by synopsis">
					</div>

					<div class="col">
						<button type="submit" class="btn btn-teal">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</form>
		</div>

		<div class="mt-5">
			@include ('partials.alerts')
			
			@if ($pages->count())
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>

							<th>Synopsis</th>

							<th>Created</th>

							<th>Delete</th>
						</tr>
					</thead>

					<tbody>
						@foreach ($pages as $page)
							<tr>
								<td>{{ $page->id }}</td>

								<td>
									<a href="{{ route('admin.pages.edit', $page) }}">{{ $page->synopsis }}</a>
								</td>

								<td>{{ date('d-m-Y', strtotime($page->created_at)) }}</td>

								<td>
									<form action="{{ route('admin.pages.destroy', $page) }}" method="POST" onsubmit="if (!confirmed('Are you sure?')) return false;">
										@csrf @method('DELETE')

										<button class="btn" type="submit">
											<i class="fa fa-trash-o"></i>
										</button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<p>No pages match your criteria.</p>
			@endif

			@if ($pages->count())
				<div class="pagination-wrapper">
					{{ $pages->appends(request()->all())->links() }}
				</div>
			@endif
		</div>
	</div>
@endsection