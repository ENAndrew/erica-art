@extends ('layouts.app')

@section ('content')
	<div class="about-wrapper">
		<section class="p-120">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 d-flex justify-content-center align-items-center">
						<img class="img-fluid mb-5" src="/img/layout/self_2.png" alt="erica andrew">
					</div>

					<div class="col-lg-8">
						<p>Hi, I'm Erica, an artist and web developer living in the Seattle area.</p>

						<p>I grew up in San Diego, getting my start in artwork with my creative mom putting drawings in my lunch box every day, and my engineer dad telling me bedtime stories about computers and space.</p>

						<p>At UCSD I started out as a physics major, but ended up with a psychology degree, with an emphasis in psychology and law.  I still use my thesis project in eyewitness memory retention as a convienient way to get out of jury duty.</p>

						<p>After spending 10 years climbing buildings and exploring disasters as a commercial insurance adjuster, I made the jump to web development by teaching myself HTML, CSS, JavaScript, and PHP. I also leveraged a bootcamp that gave me the opportunity to create projects with others determining requirements and timelines.</p>

						<p>I can also ride a horse, fly a plane, sail a boat, and drive a stickshift, just in case I ever really need a getaway. I've recently earned my amateur radio operator's license, to be prepared for overlanding adventures.</p>

						<p><a href="{{ route('contact') }}">Contact me</a> for artwork commissions, tattoo designs, web projects, or to discuss video games or why the film adaptation of Annihilation is a masterpiece.</p>

						<p><a href="{{ route('favorites') }}">Check out</a> some of my favorite authors and books.</p>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection
