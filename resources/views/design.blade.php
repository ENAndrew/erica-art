@extends ('layouts.app')

@section ('content')
	<div class="dev-wrapper py-5" style="margin-top: 75px;">
		<div class="container boxes d-flex justify-content-center">
			<div class="content-box">
				<a target="_blank" rel="noopener noreferrer" href="https://www.isolvedconnect.com/">
					<div class="box d-flex flex-column justify-content-center align-items-center">
						<picture>
							<source srcset="/img/layout/sites/connect.webp" type="image/webp">
							<img src="/img/layout/sites/connect.png" alt="isolved isolvedconnect">
						</picture>

						<p class="info">Development</p>
					</div>
				</a>
			</div>

			<div class="content-box">
				<a target="_blank" rel="noopener noreferrer" href="https://www.isolvedhcm.com">
					<div class="box d-flex flex-column justify-content-center align-items-center">
						<picture>
							<source srcset="/img/layout/sites/isolved_hcm.webp" type="image/webp">
							<img src="/img/layout/sites/isolved_hcm.png" alt="isolved HCM">
						</picture>

						<p class="info">Development</p>
					</div>
				</a>
			</div>

			<div class="content-box">
				<a target="_blank" rel="noopener noreferrer" href="https://www.isolvedpartner.com">
					<div class="box d-flex flex-column justify-content-center align-items-center">
						<picture>
							<source srcset="/img/layout/sites/isolved_network_new.webp" type="image/webp">
							<img src="/img/layout/sites/isolved_network_new.png" alt="isolved Network">
						</picture>

						<p class="mb-0">Partner Portal</p>

						<p class="info">Development</p>
					</div>
				</a>
			</div>

			<div class="content-box">
				<a target="_blank" rel="noopener noreferrer" href="https://www.isolvedbenefitservices.com/">
					<div class="box d-flex flex-column justify-content-center align-items-center">
						<picture>
							<source srcset="/img/layout/sites/IBS_positive.webp" type="image/webp">
							<img src="/img/layout/sites/IBS_positive.png" alt="isolved Benefit Services">
						</picture>

						<p class="info">Development</p>
					</div>
				</a>
			</div>

			<div class="content-box">
				<a target="_blank" rel="noopener noreferrer" href="https://www.isolvednetwork.com">
					<div class="box d-flex flex-column justify-content-center align-items-center">
						<picture>
							<source srcset="/img/layout/sites/isolved_network_new.webp" type="image/webp">
							<img src="/img/layout/sites/isolved_network_new.png" alt="isolved Network">
						</picture>

						<p class="info">Development</p>
					</div>
				</a>
			</div>

			<div class="content-box">
				<a target="_blank" rel="noopener noreferrer" href="https://bitbucket.org/ENAndrew/erica-art/src/master/">
					<div class="box d-flex flex-column justify-content-center align-items-center">
						<h3 style="color: black;">Erica Andrew Art</h3>

						<p class="mb-0">Source Code on Bitbucket</p>

						<p class="info">Development &amp; Design</p>
					</div>
				</a>
			</div>
		</div>
	</div>
@endsection
