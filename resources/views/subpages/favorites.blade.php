@extends ('layouts.app')

@section ('content')
	<div class="favorites-wrapper">
		<section style="padding-top: 120px;">
			<div class="container">
				<h2>Favorite Authors</h2>

				<h3>Jeff Vandermeer:</h3>

				<p>Patron saint of the esoteric, Vandermeer's books blend the utterly weird with poignantly mundane. I first discovered his writing with <a href="https://www.amazon.com/City-Saints-Madmen-Jeff-VanderMeer/dp/0553383574">"City of Saints and Madmen"</a> a delightfully annotated history of a city founded on the bones of a population of strange fungal creatures. I strongly recommend getting a physical copy to fully appreciate the extensive footnotes.</p>

				<p><a href="https://www.amazon.com/Borne-Novel-Jeff-VanderMeer/dp/0374115249">"Borne: A Novel"</a> ~ An enormous flying bear overshadows a city filled with discarded biotech experiments. By the first chapters of this book this feels completely rational.</p>

				<p><a href="https://www.amazon.com/Annihilation-Novel-Southern-Reach-Trilogy/dp/0374104093">"Annihilation"</a> ~ You join a scientific exploration into Area X, a topographic anomaly of unknown origin, where the concept of self is blasted by the weird and poetic effects of a force tangential to human understanding. This is a truly beautiful and desolate mystery and "some questions will ruin you if you are denied the answer long enough."</p>

				<h3>Greg Bear:</h3>

				<p>The first book I ever bought for myself was a dog-eared copy of Bear's collection of short stories <a href="https://www.amazon.com/Wind-Burning-Woman-Greg-Bear/dp/0870540947">"The Wind From a Burning Woman"</a>. Bear builds worlds where organic moving cities crawl through the remnants of human civilization, where interstellar wars are fought by genetically engineered children, a Russian-speaking teddy bear is a guide through a conjunction of bizzare realities, and stories themselves have a dark life and power.</p>

				<p>I love books that take scientific concepts and push them to extremes, and <a href="https://www.amazon.com/Blood-Music-Greg-Bear/dp/1497637023">"Blood Music"</a> is a stellar example. What would happen if the cells within our bodies could communicate with each other at the same level as the meta-world of humans? The outcome is a journey through a transformed biological world that threatens the fabric of reality.</p>

				<h3>Peter Watts:</h3>

				<p><a href="https://www.amazon.com/Blindsight-Peter-Watts/dp/0765319640">"Blindsight"</a> is another novel that takes scientific and psychological realities to the edge. How would we communicate with a truly alien life form? Who would we send to speak with them, if they even speak, if they even think? Watts explores the concept of consciousness itself while drawing on real psychological phenomena, such as the titular <a href="https://en.wikipedia.org/wiki/Blindsight">blindsight</a> where humans with damage to certain parts of the brain can see without having any concious experience of having sight. Also, there are vampires, and it makes perfect sense.</p>

				<h3>VS Ramachandran:</h3>

				<p>I was extremely lucky to have taken a course in psychology with Ramachandran, author of <a href="https://www.amazon.com/Phantoms-Brain-Probing-Mysteries-Human/dp/0688172172">"Phantoms in the Brain"</a>. Ramachandran has lead groundbreaking research into phantom limbs and other neurological disorders. You may remember the episode of House where he "cures" a disabled veteran with a mirror box, which is a treatment discovered by Ramachandran. The book explores this phenomena as well as split-brain patients, hallucinations, religious experiences, and the neurological basis of self.</p>

				<h3>More Recommendations:</h3>

				<p><a href="https://www.amazon.com/Guns-South-Novel-Harry-Turtledove/dp/0345384687">"The Guns of the South"</a> ~ What would happen if the South was provided with modern weapons during the Civil War?</p>

				<p><a href="https://www.amazon.com/Dark-Tower-I-Gunslinger/dp/1501161806">"The Gunslinger"</a> ~ The man in Black fled across the Desert, and the Gunslinger followed.</p>

				<p><a href="https://www.amazon.com/Talisman-Novel-Stephen-King/dp/145169721X">"The Talisman"</a> ~ I named my son after the protagonist of this novel. Ultimately a story of the great American Road Trip, if America existed in multiple dimensions.</p>

				<p><a href="https://www.amazon.com/Startide-Rising-Uplift-Trilogy-Book-ebook/dp/B0036S49KU">"Startide Rising"</a> ~ Spacefaring dolphins embroiled in a galactic intrigue that could bring down the galaxy.</p>

				<p><a href="https://www.amazon.com/Hyperion-Cantos-Dan-Simmons/dp/0553283685">"Hyperion"</a> ~ A pilgrimage to salvage reality to a world where buildings move backwards in time and a creature that is worshiped and feared awaits our complex band.  Epic worldbuilding and gorgeous storytelling.</p>
			</div>
		</section>

		<section>
			<div class="container currently-reading mb-5">
				<h3>Currently Reading/Listening:</h3>

				<p><a href="https://www.amazon.com/American-Elsewhere-Robert-Jackson-Bennett/dp/0316200204">"American Elsewhere"</a> ~ Robert Jackson Bennett. The town of Wink in the New Mexico desert seems too good to be true. What happens when beings from beyond space and time attempt to live among us... and run a diner, among other things. Weird eldrich horror with a side of wry comedy.</p>
			</div>
		</section>
	</div>
@endsection