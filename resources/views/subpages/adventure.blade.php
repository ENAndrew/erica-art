@extends ('layouts.app')

@section ('content')
	<div class="adventure-wrapper">
		<section class="p-120">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6">
						<adventure :pages="{{ json_encode($pages) }}"></adventure>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection